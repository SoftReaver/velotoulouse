package fr.cnam.smb116.velotoulouse.models;

import org.json.JSONException;
import org.json.JSONObject;

public class VelibStation {
    public Integer number;
    public String contract_name;
    public String name;
    public String address;
    public Position position;
    public Boolean banking;
    public Boolean bonus;
    public Integer bike_stands;
    public Integer available_bike_stands;
    public Integer available_bikes;
    public String status;
    public Long last_update;

    public static VelibStation parseJSONToObject(JSONObject jsonObject) throws JSONException {
        VelibStation velibStation = new VelibStation();
        velibStation.number = (Integer) jsonObject.get("number");
        velibStation.contract_name = (String) jsonObject.get("contract_name");
        velibStation.name = (String) jsonObject.get("name");
        velibStation.address = (String) jsonObject.get("address");
        velibStation.position = new Position();
        velibStation.position.lat = (Double) ((JSONObject) jsonObject.get("position")).get("lat");
        velibStation.position.lng = (Double) ((JSONObject) jsonObject.get("position")).get("lng");
        velibStation.banking = (Boolean) jsonObject.get("banking");
        velibStation.bonus = (Boolean) jsonObject.get("bonus");
        velibStation.bike_stands = (Integer) jsonObject.get("bike_stands");
        velibStation.available_bike_stands = (Integer) jsonObject.get("available_bike_stands");
        velibStation.available_bikes = (Integer) jsonObject.get("available_bikes");
        velibStation.status = (String) jsonObject.get("status");
        velibStation.last_update = (Long) jsonObject.get("last_update");
        return velibStation;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
