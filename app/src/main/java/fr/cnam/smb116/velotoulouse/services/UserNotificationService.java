package fr.cnam.smb116.velotoulouse.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import fr.cnam.smb116.velotoulouse.R;

public class UserNotificationService extends Service {
    private static final String CHANNEL_ID = "VeloToulouse";
    private static final String CHANNEL_NAME = "VeloToulouse";
    private static final String CHANNEL_DESCRIPTION = "Notifications concernant le réseau de vélo libre service de la ville de Toulouse";

    private IBinder localBinder;
    private NotificationManagerCompat notificationManager;
    private NotificationCompat.Builder notificationBuilder;

    public UserNotificationService() {
        this.localBinder = new MyBinder();
    }

    public void notifyUser(int id, String title, String text) {
        this.notificationManager.cancel(id);
        this.notificationBuilder.setContentTitle(title);
        this.notificationBuilder.setContentText(text);
        this.notificationManager.notify(id, this.notificationBuilder.build());
    }

    public void removeNotification(int id) {
        this.notificationManager.cancel(id);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return this.localBinder;
    }

    public class MyBinder extends Binder {
        public UserNotificationService getService(Context context) {
            createNotificationChannel(context);
            if (notificationManager == null) {
                notificationManager = NotificationManagerCompat.from(context);
            }

            if (notificationBuilder == null) {
                notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_user_notif)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setDefaults(NotificationCompat.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            }

            return UserNotificationService.this;
        }
    }

    private void createNotificationChannel(Context context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = CHANNEL_NAME;
            String description = CHANNEL_DESCRIPTION;
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            channel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            channel.enableVibration(true);
            channel.enableLights(true);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
