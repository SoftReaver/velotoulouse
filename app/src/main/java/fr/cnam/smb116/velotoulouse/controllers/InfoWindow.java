package fr.cnam.smb116.velotoulouse.controllers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import fr.cnam.smb116.velotoulouse.R;
import fr.cnam.smb116.velotoulouse.models.VelibStation;

public class InfoWindow implements GoogleMap.InfoWindowAdapter {
    private Context context;
    private View mContents;

    public InfoWindow(Context context) {
        this.context = context;
        mContents = LayoutInflater.from(context).inflate(R.layout.info_window_content, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        render(marker, mContents);
        return mContents;
    }

    private void render(Marker marker, View view) {
        VelibStation velibStation = (VelibStation) marker.getTag();
        //info window creation

        TextView titleUi = ((TextView) view.findViewById(R.id.stationTitleText));
        titleUi.setText(context.getString(R.string.title, velibStation.name));

        TextView statusUi = ((TextView) view.findViewById(R.id.stationStatusText));
        if (velibStation.status.equals("OPEN")) {
            statusUi.setTextColor(context.getResources().getColor(R.color.green));
            statusUi.setText(context.getString(R.string.open));
        } else if(velibStation.status.equals("CLOSE")) {
            statusUi.setTextColor(context.getResources().getColor(R.color.red));
            statusUi.setText(context.getString(R.string.close));
        } else { // Unknown
            statusUi.setTextColor(context.getResources().getColor(R.color.gray));
            statusUi.setText(context.getString(R.string.unknown));
        }

        //Last update time
        Long lastUpdate = (System.currentTimeMillis() - velibStation.last_update) / 60_000;
        TextView lastUpdateUi = ((TextView) view.findViewById(R.id.lastUpdateText));
        lastUpdateUi.setText(context.getString(R.string.last_update, lastUpdate));

        //available bikes
        TextView availableBikesUi = ((TextView) view.findViewById(R.id.availableBikesText));
        availableBikesUi.setText(context.getString(R.string.available_bikes, velibStation.available_bikes));

        //available bike slots
        TextView availableBikeSlotsUi = ((TextView) view.findViewById(R.id.availableSlotsText));
        availableBikeSlotsUi.setText(context.getString(R.string.available_slots, velibStation.available_bike_stands));

        //available banking
        TextView availableBankingUi = ((TextView) view.findViewById(R.id.bankingText));
        availableBankingUi.setText(context.getString(R.string.banking, velibStation.banking? context.getString(R.string.yes) : context.getString(R.string.no)));

        //Station position
        TextView positionUi = ((TextView) view.findViewById(R.id.positionText));
        positionUi.setText(context.getString(R.string.position, velibStation.position.lat, velibStation.position.lng));
    }
}
