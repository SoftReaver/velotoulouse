package fr.cnam.smb116.velotoulouse.models;

public class VeloToulouseNotification {
    public int stationNumber;
    public boolean isWatchingForAvailableBike;
    public boolean availableBikeLastBooleanValue;
    public int availableBikeThreshold;
    public boolean isWatchingForAvailableSlot;
    public boolean availableSlotLastBooleanValue;
    public int availableSlotThreshold;
}
