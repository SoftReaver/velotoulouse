package fr.cnam.smb116.velotoulouse.models;

public class SuggestionVelibStation implements Comparable<SuggestionVelibStation>{
    public float similarity;
    public VelibStation velibStation;

    public SuggestionVelibStation(float similarity, VelibStation velibStation) {
        this.similarity = similarity;
        this.velibStation = velibStation;
    }

    @Override
    public int compareTo(SuggestionVelibStation candidate) {
        return (this.similarity < candidate.similarity ? -1 :
                (this.similarity == candidate.similarity ? 0 : 1));
    }
}
