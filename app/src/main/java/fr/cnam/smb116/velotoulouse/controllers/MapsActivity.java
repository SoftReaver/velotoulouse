package fr.cnam.smb116.velotoulouse.controllers;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import fr.cnam.smb116.velotoulouse.R;
import fr.cnam.smb116.velotoulouse.databinding.ActivityMapsBinding;
import fr.cnam.smb116.velotoulouse.models.SuggestionVelibStation;
import fr.cnam.smb116.velotoulouse.models.VelibStation;
import fr.cnam.smb116.velotoulouse.models.VeloToulouseNotification;
import fr.cnam.smb116.velotoulouse.services.UserNotificationService;
import fr.cnam.smb116.velotoulouse.services.VeloToulouseDAO;

public class MapsActivity extends FragmentActivity
        implements OnMapReadyCallback,
        Notifiable<ArrayList<VelibStation>>,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnInfoWindowLongClickListener,
        GoogleMap.OnInfoWindowCloseListener,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback
{
    private GoogleMap mMap;
    private ArrayList<Marker> markers;
    private ActivityMapsBinding binding;
    private ArrayList<VelibStation> velibStationsList;
    private ArrayList<VelibStation> suggestionList;
    private Map<Integer, VeloToulouseNotification> watchers;

    private Boolean isBound = true;
    private VeloToulouseDAO veloToulouseDAO;
    private Boolean isNotificationServiceBound;
    private UserNotificationService userNotificationService;
    private static Integer refreshInterval  = 30_000; //Refresh rate for data in ms
    private Integer openedStationNumber;
    private Marker selectedMarker;
    private SearchView searchView;
    private ListView suggestionListView;

    private ConstraintLayout mBottomSheetConstraintLayout;
    private BottomSheetBehavior mBottomSheetBehavior;

    // Bottomsheet header views
    private ListView mWatchStationListview;
    private LinearLayout mWatchControlLayout;
    private ImageView mAvailableBikeStateImageView;
    private ImageView mAvailableSlotStateImageView;
    private Switch mAvailableBikeSwitch;
    private Switch mAvailableSlotSwitch;
    private TextView mBikeThresholdTextView;
    private TextView mSlotThresholdTextView;
    private TextView mStationNameTextView;
    private SeekBar mBikeSeekbar;
    private SeekBar mSlotSeekbar;
    private ImageView notificationImageview;

    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean permissionDenied = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mBottomSheetConstraintLayout = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(mBottomSheetConstraintLayout);

        velibStationsList = new ArrayList<>();
        watchers = new HashMap<>();
        isBound = false;
        isNotificationServiceBound = false;
        openedStationNumber = -1;
        markers = new ArrayList<>();

        // Bottomsheet header views
        mWatchStationListview = findViewById(R.id.watch_station_list_view);
        mWatchControlLayout = findViewById(R.id.watch_control_panel_layout);
        mWatchControlLayout.setVisibility(View.INVISIBLE);

        mAvailableBikeStateImageView = findViewById(R.id.available_bike_state);
        mAvailableSlotStateImageView = findViewById(R.id.available_slot_state);
        mAvailableBikeSwitch = findViewById(R.id.watch_bike_switch);
        mAvailableBikeSwitch.setOnClickListener((buttonView) -> {
            if (selectedMarker != null) {
                VelibStation station = (VelibStation) selectedMarker.getTag();
                if (MapsActivity.this.watchers.containsKey(station.number)) {
                    VeloToulouseNotification watcher = watchers.get(station.number);
                    watcher.isWatchingForAvailableBike = ((Switch)buttonView).isChecked();
                    watcher.availableBikeThreshold = mBikeSeekbar.getProgress() + 1;
                    mBikeThresholdTextView.setText("" + (mBikeSeekbar.getProgress() + 1));
                    if (watcher.availableBikeThreshold <= station.available_bikes) {
                        watcher.availableBikeLastBooleanValue = true;
                    } else {
                        watcher.availableBikeLastBooleanValue = false;
                    }
                    syncBottomsheet();
                }
            }
        });
        mAvailableSlotSwitch = findViewById(R.id.watch_slot_switch);
        mAvailableSlotSwitch.setOnClickListener((buttonView) -> {
            if (selectedMarker != null) {
                VelibStation station = (VelibStation) selectedMarker.getTag();
                if (MapsActivity.this.watchers.containsKey(station.number)) {
                    VeloToulouseNotification watcher = watchers.get(station.number);
                    watcher.isWatchingForAvailableSlot = ((Switch)buttonView).isChecked();
                    watcher.availableSlotThreshold = mSlotSeekbar.getProgress() + 1;
                    mSlotThresholdTextView.setText("" + (mSlotSeekbar.getProgress() + 1));
                    if (watcher.availableSlotThreshold <= station.available_bike_stands) {
                        watcher.availableSlotLastBooleanValue = true;
                    } else {
                        watcher.availableSlotLastBooleanValue = false;
                    }
                    syncBottomsheet();
                }
            }
        });
        mBikeThresholdTextView = findViewById(R.id.available_bike_threshold_textview);
        mSlotThresholdTextView = findViewById(R.id.available_slot_threshold_textview);
        mStationNameTextView = findViewById(R.id.station_name_textview);
        mStationNameTextView.setText("Veuillez selectionner une station sur la carte");

        mBikeSeekbar = findViewById(R.id.available_bike_seekbar);
        mBikeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (selectedMarker != null && fromUser) {
                    VelibStation station = (VelibStation) selectedMarker.getTag();
                    VeloToulouseNotification watcher = watchers.get(station.number);
                    watcher.availableBikeThreshold = progress + 1;
                    mBikeThresholdTextView.setText("" + (progress + 1));
                    if (watcher.availableBikeThreshold <= station.available_bikes) {
                        watcher.availableBikeLastBooleanValue = true;
                    } else {
                        watcher.availableBikeLastBooleanValue = false;
                    }
                    syncBottomsheet();
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        mSlotSeekbar = findViewById(R.id.available_slot_seekbar);
        mSlotSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (selectedMarker != null && fromUser) {
                    VelibStation station = (VelibStation) selectedMarker.getTag();
                    VeloToulouseNotification watcher = watchers.get(station.number);
                    watcher.availableSlotThreshold = progress + 1;
                    mSlotThresholdTextView.setText("" + (progress + 1));
                    if (watcher.availableSlotThreshold <= station.available_bike_stands) {
                        watcher.availableSlotLastBooleanValue = true;
                    } else {
                        watcher.availableSlotLastBooleanValue = false;
                    }
                    syncBottomsheet();
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        notificationImageview = findViewById(R.id.notification_image_view);

        searchView = findViewById(R.id.search_view);
        suggestionList = new ArrayList<>();
        ArrayAdapter<VelibStation> adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, suggestionList);
        suggestionListView = findViewById(R.id.suggestion_list_view);
        suggestionListView.setAdapter(adapter);

        suggestionListView.setOnItemClickListener((parent, view, position, id) -> {
            VelibStation selectedStation = suggestionList.get(position);
            for (Marker marker : markers) {
                if (selectedStation.number.equals( ((VelibStation) marker.getTag()).number )) {
                    searchView.clearFocus();
                    suggestionList.clear();
                    ArrayAdapter<VelibStation> adapt = new ArrayAdapter(MapsActivity.this, android.R.layout.simple_list_item_1, suggestionList);
                    suggestionListView.setAdapter(adapt);

                    marker.showInfoWindow();
                    // move camera to marker
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 15));

                    // Fake a "click on marker" event
                    onMarkerClick(marker);
                    break;
                }
            }
            searchView.setQuery("", false);
        });

        // adding on query listener for our search view.
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                suggestionList.clear();
                // on below line we are getting the
                // location name from search view.
                String location = searchView.getQuery().toString();

                // checking if the entered location is null or not.
                if (location != null && location.length() > 0) {
                    ArrayList<SuggestionVelibStation> suggestions = new ArrayList<>();
                    for (VelibStation velibStation : velibStationsList) {
                        float similarity = 0.0f;
                        if (velibStation.name.toLowerCase().indexOf(location.toLowerCase()) != -1) {
                            similarity = 1.0f;
                        } else {
                            similarity = stringLooksLikeThreshold(location, velibStation.address);
                        }
                        if (similarity > 0.2f) {
                            suggestions.add(new SuggestionVelibStation(similarity, velibStation));
                        }
                    }
                    Collections.sort(suggestions);
                    for (int i = suggestions.size() - 1; i >= 0; --i) {
                        suggestionList.add(suggestions.get(i).velibStation);
                    }
                }
                ArrayAdapter<VelibStation> adapter = new ArrayAdapter(MapsActivity.this, android.R.layout.simple_list_item_1, suggestionList);
                suggestionListView.setAdapter(adapter);
                return true;
            }
        });

        mBottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        this.bindUserNotificationService();
        this.bindDAOService();

        /** Update velib list every 'refreshInterval' milliseconds */
        new Timer().scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                if (isBound) {
                    veloToulouseDAO.getAllVelibStations(MapsActivity.this);
                } else {
                    // Try to bind service again
                    MapsActivity.this.bindDAOService();
                }
            }
        },0,refreshInterval);
    }

    /**
     * Returns a float number representing the similarity ratio between both subjects passed.
     * This method is not case sensitive
     *
     * @param subject1 user entry string
     * @param subject2 model
     * @Return a float number between 0.0 and 1.0
     */
    private float stringLooksLikeThreshold(String subject1, String subject2) {
        String[] split1 = subject1.toLowerCase().split(" ");
        String[] split2 = subject2.toLowerCase().split(" ");
        int count = 0;
        for (String word : split1) {
            for(String word2 : split2) {
                if (word.equals(word2) || word2.indexOf(word) != -1) {
                    count++;
                }
            }
        }
        float hitRatio = (float) count / split1.length;
        float lengthRatio = (split1.length / split2.length) * 0.25f;
        return hitRatio - (0.25f - lengthRatio);
    }

    private void bindDAOService() {
        Intent intent = new Intent(this , VeloToulouseDAO.class);
        startService(intent);
        bindService(intent, boundServiceConnection, BIND_AUTO_CREATE);
    }

    private void bindUserNotificationService() {
        Intent intent = new Intent(this , UserNotificationService.class);
        startService(intent);
        bindService(intent, notificationServiceConnection, BIND_AUTO_CREATE);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.setInfoWindowAdapter(new InfoWindow(this));

        // Move the camera to the map coordinates and zoom in closer.
        LatLng toulouse = new LatLng(43.60, 1.455);
        LatLng toulouseSW = new LatLng(43.50, 1.266);
        LatLng toulouseNE = new LatLng(43.683, 1.530);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(toulouse));
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(13));

        // Set listeners for marker events.  See the bottom of this class for their behavior.
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnInfoWindowCloseListener(this);
        mMap.setOnInfoWindowLongClickListener(this);

        // Override the default content description on the view, for accessibility mode.
        // Ideally this string would be localised.
        mMap.setContentDescription("Carte des stations vélib de Toulouse");

        // Define camera limits
        LatLngBounds bounds = new LatLngBounds.Builder()
                .include(toulouseSW)
                .include(toulouseNE)
                .build();
        mMap.setLatLngBoundsForCameraTarget(bounds);

        // refresh stations data on map if any
        this.syncStationsWithMap();

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        enableMyLocation();
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                View locationButton = ((View) findViewById(R.id.map).findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                // position on right bottom
                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                rlp.setMargins(0, 180, 180, 0);
            }
        } else {
            // Permission to access the location is missing. Show rationale and request permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Permission was denied. Display an error message
            // Display the missing permission error dialog when the fragments resume.
            permissionDenied = true;
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (permissionDenied) {
            // Permission was not granted, display error dialog.
            Toast.makeText(this, "La géolocalisation n'est pas disponible", Toast.LENGTH_LONG).show();
            permissionDenied = false;
        }
    }

    /**
     * Called once VeloToulouseDAO retrieved velib stations
     *
     * @param data
     */
    @Override
    public void notify(ArrayList<VelibStation> data) {
        if (data == null) {
            mStationNameTextView.setText("Aucune donnée concernant les stations de vélo");
        } else {
            velibStationsList = data;
            syncBottomsheet();
            if (this.mMap != null) {
                this.syncStationsWithMap();
            }
            if (isNotificationServiceBound) {
                syncWatchersAndNotify();
            } else {
                // Try to bind service again
                MapsActivity.this.bindUserNotificationService();
            }
        }
    }

    /**
     * Refresh the map with Velib stations.
     * Draw all markers on map.
     */
    private void syncStationsWithMap() {
        // Clear the map because we don't markers to be duplicated.
        Integer openedStationNumber = this.openedStationNumber;
        mMap.clear();
        markers.clear();

        for(VelibStation velibStation : this.velibStationsList) {
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(velibStation.position.lat, velibStation.position.lng))
                    .title(velibStation.name)
                    .snippet("Snippet"));
            marker.setTag(velibStation);

            markers.add(marker);

            VeloToulouseNotification watcher = watchers.get(velibStation.number);
            if (watcher != null && (watcher.isWatchingForAvailableSlot || watcher.isWatchingForAvailableBike)) {
                marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            }

            // If info window was opened before data refresh, then reopen it
            if (openedStationNumber.equals(velibStation.number)) {
                this.selectedMarker = marker;
                this.openedStationNumber = velibStation.number;

                marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                marker.showInfoWindow();
                onMarkerClick(marker);
            }
        }
    }

    /**
     * Synchronize and notify if a watcher condition change
     */
    private void syncWatchersAndNotify() {
        for (VelibStation station : velibStationsList) {
            if (watchers.containsKey(station.number)) {
                VeloToulouseNotification watcher = watchers.get(station.number);
                if (watcher.isWatchingForAvailableBike) {
                    if (watcher.availableBikeThreshold <= station.available_bikes && watcher.availableBikeLastBooleanValue == false) {
                        watcher.availableBikeLastBooleanValue = true;
                        userNotificationService.notifyUser(station.number, station.name + " - vélo disponible", "Il y a au moins " + watcher.availableBikeThreshold + " vélo(s) disponible(s).");
                    } else if (watcher.availableBikeThreshold > station.available_bikes && watcher.availableBikeLastBooleanValue == true) {
                        watcher.availableBikeLastBooleanValue = false;
                        userNotificationService.notifyUser(station.number, station.name + " - pas de vélo disponible", "Il n'y a pas assez de vélo pour la station '" + station.name + "'. (" + station.available_bike_stands + " disponible(s) sur " + watcher.availableSlotThreshold + " demandé).");
                    }
                }
                if (watcher.isWatchingForAvailableSlot) {
                    if (watcher.availableSlotThreshold <= station.available_bike_stands && watcher.availableSlotLastBooleanValue == false) {
                        watcher.availableSlotLastBooleanValue = true;
                        userNotificationService.notifyUser(station.number + 1000, station.name + " - place disponible", "Il y a au moins " + watcher.availableSlotThreshold + " place(s) disponible(s).");
                    } else if (watcher.availableSlotThreshold > station.available_bike_stands && watcher.availableSlotLastBooleanValue == true) {
                        watcher.availableSlotLastBooleanValue = false;
                        userNotificationService.notifyUser(station.number + 1000, station.name + " - pas de place disponible", "Il n'y a pas assez de place pour la station '" + station.name + "'. (" + station.available_bike_stands + " disponible(s) sur " + watcher.availableSlotThreshold + " demandé).");
                    }
                }
            } else {
                VeloToulouseNotification newWatcher = new VeloToulouseNotification();
                newWatcher.isWatchingForAvailableSlot = false;
                newWatcher.isWatchingForAvailableBike = false;
                newWatcher.availableBikeLastBooleanValue = false;
                newWatcher.availableSlotLastBooleanValue = false;
                newWatcher.stationNumber = station.number;
                watchers.put(station.number, newWatcher);
            }
        }
    }

    private final ServiceConnection boundServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            VeloToulouseDAO.MyBinder binderBridge = (VeloToulouseDAO.MyBinder) service;
            veloToulouseDAO = binderBridge.getService();
            veloToulouseDAO.setContext(MapsActivity.this);
            isBound = true;

            veloToulouseDAO.getAllVelibStations(MapsActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
            veloToulouseDAO = null;
        }
    };

    private final ServiceConnection notificationServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            UserNotificationService.MyBinder binderBridge = (UserNotificationService.MyBinder) service;
            userNotificationService = binderBridge.getService(MapsActivity.this);

            isNotificationServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isNotificationServiceBound = false;
            userNotificationService = null;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(isBound){
            unbindService(boundServiceConnection);
            isBound = false;
        }
        if (isNotificationServiceBound) {
            unbindService(notificationServiceConnection);
            isNotificationServiceBound = false;
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if (MapsActivity.this.selectedMarker != null) {
            MapsActivity.this.selectedMarker.hideInfoWindow();
        }
    }

    @Override
    public void onInfoWindowClose(Marker marker) {
        VelibStation station = (VelibStation) marker.getTag();
        if (MapsActivity.this.selectedMarker != null) {
            VeloToulouseNotification watcher = watchers.get(station.number);
            if (watcher != null && (watcher.isWatchingForAvailableSlot || watcher.isWatchingForAvailableBike)) {
                MapsActivity.this.selectedMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            } else {
                MapsActivity.this.selectedMarker.setIcon(BitmapDescriptorFactory.defaultMarker());
            }
        }
        MapsActivity.this.selectedMarker = null;
        MapsActivity.this.openedStationNumber = -1;

        // Update bottomsheetHeader
        mWatchControlLayout.setVisibility(View.INVISIBLE);
        mStationNameTextView.setText("Aucune station selectionnée");
    }

    @Override
    public void onInfoWindowLongClick(Marker marker) {
        // NOP
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        VelibStation station = (VelibStation) marker.getTag();
        MapsActivity.this.selectedMarker = marker;
        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));

        // Keep track on the selected station infoWindow
        MapsActivity.this.openedStationNumber = ((VelibStation) marker.getTag()).number;

        // Update bottomsheetHeader
        mWatchControlLayout.setVisibility(View.VISIBLE);
        mStationNameTextView.setText(station.name);
        syncBottomsheet();

        // We return false to indicate that we have not consumed the event and that we wish
        // for the default behavior to occur (which is for the camera to move such that the
        // marker is centered and for the marker's info window to open, if it has one).
        return false;
    }

    /**
     * Move map camera to the station marker and open info window.
     * If station number does not exist, nothing happen.
     *
     * @param stationNumber
     */
    public void showStation(int stationNumber) {
        // Look for marker
        for (Marker marker : markers) {
            if (((VelibStation)marker.getTag()).number == stationNumber) {
                marker.showInfoWindow();
                // move camera to marker
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 15));
                onMarkerClick(marker);
                break;
            }
        }
    }

    /**
     * Refresh the content of the bottomsheet view
     */
    private void syncBottomsheet() {
        if (selectedMarker != null) {
            // Update header control panel
            VelibStation station = (VelibStation) selectedMarker.getTag();
            VeloToulouseNotification watcher = watchers.get(station.number);
            mAvailableBikeSwitch.setChecked(watcher.isWatchingForAvailableBike);
            mAvailableSlotSwitch.setChecked(watcher.isWatchingForAvailableSlot);

            mBikeSeekbar.setMax(station.bike_stands - 1);
            if(watcher.availableBikeThreshold <= 0) {
                mBikeSeekbar.setProgress(0);
            } else {
                mBikeSeekbar.setProgress(watcher.availableBikeThreshold - 1);
            }
            mBikeThresholdTextView.setText("" + (mBikeSeekbar.getProgress() + 1));

            mSlotSeekbar.setMax(station.bike_stands - 1);
            if(watcher.availableSlotThreshold <= 0) {
                mSlotSeekbar.setProgress(0);
            } else {
                mSlotSeekbar.setProgress(watcher.availableSlotThreshold - 1);
            }
            mSlotThresholdTextView.setText("" + (mSlotSeekbar.getProgress() + 1));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // Version for SDK API >= 21
                if (watcher.isWatchingForAvailableBike) {
                    if (watcher.availableBikeThreshold <= station.available_bikes) {
                        mAvailableBikeStateImageView.setImageDrawable(getDrawable(R.drawable.ic_green_dot_foreground));
                    } else {
                        mAvailableBikeStateImageView.setImageDrawable(getDrawable(R.drawable.ic_red_dot_foreground));
                    }
                } else {
                    mAvailableBikeStateImageView.setImageDrawable(getDrawable(R.drawable.ic_gray_dot_foreground));
                }
                if (watcher.isWatchingForAvailableSlot) {
                    if (watcher.availableSlotThreshold <= station.available_bike_stands) {
                        mAvailableSlotStateImageView.setImageDrawable(getDrawable(R.drawable.ic_green_dot_foreground));
                    } else {
                        mAvailableSlotStateImageView.setImageDrawable(getDrawable(R.drawable.ic_red_dot_foreground));
                    }
                } else {
                    mAvailableSlotStateImageView.setImageDrawable(getDrawable(R.drawable.ic_gray_dot_foreground));
                }

            } else { // API SDK < 21
                if (watcher.isWatchingForAvailableBike) {
                    if (watcher.availableBikeThreshold <= station.available_bikes) {
                        mAvailableBikeStateImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_green_dot_foreground));
                    } else {
                        mAvailableBikeStateImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_red_dot_foreground));
                    }
                } else {
                    mAvailableBikeStateImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_green_dot_foreground));
                }
                if (watcher.isWatchingForAvailableSlot) {
                    if (watcher.availableSlotThreshold <= station.available_bike_stands) {
                        mAvailableSlotStateImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_green_dot_foreground));
                    } else {
                        mAvailableSlotStateImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_red_dot_foreground));
                    }
                } else {
                    mAvailableSlotStateImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_green_dot_foreground));
                }

            }
        }
        // Refresh the watchers list
        ArrayList<VeloToulouseNotification> arrayList = new ArrayList<>();
        for (VeloToulouseNotification watcher : watchers.values()) {
            if (watcher.isWatchingForAvailableSlot || watcher.isWatchingForAvailableBike) {
                arrayList.add(watcher);
            }
        }
        mWatchStationListview.setAdapter(new WatcherListItemAdapter(MapsActivity.this, arrayList));
    }

    /**
     * Get a velib station by number
     *
     * @param number
     * @return
     */
    public VelibStation getStationByNumber(int number) {
        for (VelibStation station : velibStationsList) {
            if (station.number == number) {
                return station;
            }
        }
        return null;
    }

    /**
     * Remove the watcher for the given station number
     *
     * @param stationNumber
     */
    public void removeWatcher(int stationNumber) {
        for(VeloToulouseNotification watcher : watchers.values()) {
            if (watcher.stationNumber == stationNumber) {
                watcher.isWatchingForAvailableBike = false;
                watcher.availableBikeLastBooleanValue = false;
                watcher.isWatchingForAvailableSlot = false;
                watcher.availableSlotLastBooleanValue = false;
                userNotificationService.removeNotification(stationNumber);

                for (Marker marker : markers) {
                    VelibStation station = (VelibStation) marker.getTag();
                    if (station.number == stationNumber) {
                        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                        break;
                    }
                }

                syncBottomsheet();
                Toast.makeText(this, "Notification retirée", Toast.LENGTH_LONG).show();
                break;
            }
        }
    }
}
