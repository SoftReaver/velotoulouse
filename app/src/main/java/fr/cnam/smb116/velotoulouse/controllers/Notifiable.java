package fr.cnam.smb116.velotoulouse.controllers;

public interface Notifiable<T> {
    public void notify(T data);
}
