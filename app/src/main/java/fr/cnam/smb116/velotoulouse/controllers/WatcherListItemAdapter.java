package fr.cnam.smb116.velotoulouse.controllers;

import android.content.Context;
import android.os.Build;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fr.cnam.smb116.velotoulouse.R;
import fr.cnam.smb116.velotoulouse.controllers.MapsActivity;
import fr.cnam.smb116.velotoulouse.models.VelibStation;
import fr.cnam.smb116.velotoulouse.models.VeloToulouseNotification;

public class WatcherListItemAdapter extends BaseAdapter {

    MapsActivity context;
    ArrayList<VeloToulouseNotification> data;
    private static LayoutInflater inflater = null;

    public WatcherListItemAdapter(MapsActivity context, ArrayList<VeloToulouseNotification> data) {
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null) {
            vi = inflater.inflate(R.layout.watcher_row, null);
        }
        VeloToulouseNotification watcher = data.get(position);
        VelibStation station = context.getStationByNumber(watcher.stationNumber);

        LinearLayout rowLayout = vi.findViewById(R.id.watcher_row_layout);
        rowLayout.setOnClickListener(v -> context.showStation(station.number));

        TextView mStationNameTextView = vi.findViewById(R.id.watcher_stationname_textview);
        ImageView mBikeStateImageview = vi.findViewById(R.id.watcher_bike_state_imageview);
        TextView mBikeAvailableTextview = vi.findViewById(R.id.watcher_available_bike_textview);
        ImageView mSlotStateImageview = vi.findViewById(R.id.watcher_slot_state_imageview);
        TextView mSlotAvailableTextview = vi.findViewById(R.id.watcher_available_slot_textview);
        ImageView mCloseButtonImageview = vi.findViewById(R.id.remove_watcher_imageview);

        mStationNameTextView.setText(station.name);

        // Update iamges
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // Version for SDK API >= 21
            if (watcher.isWatchingForAvailableBike) {
                if (watcher.availableBikeThreshold <= station.available_bikes) {
                    mBikeStateImageview.setImageDrawable(context.getDrawable(R.drawable.ic_green_dot_foreground));
                } else {
                    mBikeStateImageview.setImageDrawable(context.getDrawable(R.drawable.ic_red_dot_foreground));
                }
            } else {
                mBikeStateImageview.setImageDrawable(context.getDrawable(R.drawable.ic_gray_dot_foreground));
            }
            if (watcher.isWatchingForAvailableSlot) {
                if (watcher.availableSlotThreshold <= station.available_bike_stands) {
                    mSlotStateImageview.setImageDrawable(context.getDrawable(R.drawable.ic_green_dot_foreground));
                } else {
                    mSlotStateImageview.setImageDrawable(context.getDrawable(R.drawable.ic_red_dot_foreground));
                }
            } else {
                mSlotStateImageview.setImageDrawable(context.getDrawable(R.drawable.ic_gray_dot_foreground));
            }
            mCloseButtonImageview.setImageDrawable(context.getDrawable(R.drawable.ic_close_button));

        } else { // API SDK < 21
            if (watcher.isWatchingForAvailableBike) {
                if (watcher.availableBikeThreshold <= station.available_bikes) {
                    mBikeStateImageview.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_green_dot_foreground));
                } else {
                    mBikeStateImageview.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_red_dot_foreground));
                }
            } else {
                mBikeStateImageview.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_green_dot_foreground));
            }
            if (watcher.isWatchingForAvailableSlot) {
                if (watcher.availableSlotThreshold <= station.available_bike_stands) {
                    mSlotStateImageview.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_green_dot_foreground));
                } else {
                    mSlotStateImageview.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_red_dot_foreground));
                }
            } else {
                mSlotStateImageview.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_green_dot_foreground));
            }
            mCloseButtonImageview.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_close_button));
        }

        mBikeAvailableTextview.setText("Vélo dispo : (" + station.available_bikes + "/" + watcher.availableBikeThreshold + ")");
        mSlotAvailableTextview.setText("Place dispo : (" + station.available_bike_stands + "/" + watcher.availableSlotThreshold + ")");

        mCloseButtonImageview.setOnClickListener(v -> context.removeWatcher(watcher.stationNumber));
        return vi;
    }
}
