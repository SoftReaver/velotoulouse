package fr.cnam.smb116.velotoulouse.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import fr.cnam.smb116.velotoulouse.controllers.Notifiable;
import fr.cnam.smb116.velotoulouse.models.VelibStation;

public class VeloToulouseDAO extends Service {
    private HTTPRequesterQueue httpRequesterQueue;
    private final IBinder localBinder;

    //Données du service en ligne
    private static String  APIUrl = "https://api.jcdecaux.com/vls/v1/stations?contract=Toulouse&apiKey=";
    private static String  APIKey = "b3b0187b8e554ae38d5c1407d06f8bd90cd906fb";

    public VeloToulouseDAO() {
        localBinder = new MyBinder();
    }

    public void setContext(Context context) {
        httpRequesterQueue = new HTTPRequesterQueue(context);
    }

    public void getAllVelibStations(Notifiable<ArrayList<VelibStation>> callback) {
        if (httpRequesterQueue == null) {
            throw new RuntimeException("No context set for VeloToulouseDAO service");
        }
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, APIUrl + APIKey, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<VelibStation> velibStations = new ArrayList<>();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                velibStations.add(VelibStation.parseJSONToObject(response.getJSONObject(i)));
                            } catch (JSONException e) {
                                callback.notify(null);
                            }
                        }
                        callback.notify(velibStations);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.notify(null);
                    }
                });

        // Access the RequestQueue through your singleton class.
        httpRequesterQueue.addToRequestQueue(jsonArrayRequest);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    public class MyBinder extends Binder {

        public VeloToulouseDAO getService() {
            return VeloToulouseDAO.this;

        }
    }

    private class HTTPRequesterQueue {
        private RequestQueue requestQueue;
        private Context ctx;

        public HTTPRequesterQueue(Context context) {
            ctx = context;
            requestQueue = getRequestQueue();
        }

        public RequestQueue getRequestQueue() {
            if (requestQueue == null) {
                requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
            }
            return requestQueue;
        }

        public <T> void addToRequestQueue(Request<T> req) {
            getRequestQueue().add(req);
        }
    }
}
